﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 4f;
    public float minX, maxX;

    [SerializeField]
    private GameObject pbBullet;
    [SerializeField]
    private Transform shootPoint;

    public float shootTimer = 0.5f;
    private float currentShootTimer;
    private bool isShoot;

    void Start()  {
        currentShootTimer = shootTimer;
    }

   
    void Update() {

        MovePlayer();
        Shoot();

    }

    void MovePlayer() {

        Vector3 temp = transform.position;

        if(Input.GetAxisRaw("Horizontal") > 0f) {
           
            temp.x += speed * Time.deltaTime;

            if (temp.x > maxX)
                temp.x = maxX;

            transform.position = temp;

        } else if(Input.GetAxisRaw("Horizontal") < 0f) {

            temp.x -= speed * Time.deltaTime;

            if (temp.x < minX)
                temp.x = minX;

            transform.position = temp;

        }
    }

    void Shoot() {

        shootTimer += Time.deltaTime;

        if (shootTimer > currentShootTimer)
            isShoot = true;

        if (Input.GetButtonDown("Fire1")) {
     
            if (isShoot) {
                isShoot = false;
                shootTimer = 0;
                Instantiate(pbBullet, shootPoint.position, Quaternion.identity);
            }
        }
            
    }

}
