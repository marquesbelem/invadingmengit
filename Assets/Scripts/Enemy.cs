﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [SerializeField]
    private GameObject pbBullet;
    [SerializeField]
    private Transform shootPoint;


    void Start() {
        //InvokeRepeating("Shoot", 2.0f, 8.5f);
    }

    
    void Shoot() {
        Instantiate(pbBullet, shootPoint.position, Quaternion.identity);
    }
}
