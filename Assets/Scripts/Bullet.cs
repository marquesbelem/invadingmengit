﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 5f;
    public float destroyTimer = 3f;
    public bool shootUp;
    
    void Start() {
        Invoke("DestroyBullet", destroyTimer);
    }

   
    void Update() {
        Move();
    }

    void Move() {

        Vector3 temp = transform.position;

        if(shootUp)
            temp.y += speed * Time.deltaTime;
        else
            temp.y -= speed * Time.deltaTime;

        transform.position = temp;

    }

    void DestroyBullet() {
        Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D collision) {

        if (collision.tag == "Enemy") {
            if (shootUp) {
                Destroy(collision.gameObject);
                DestroyBullet();
            }
        }

        if (collision.tag == "Player") {
            if (!shootUp) {
                Destroy(collision.gameObject);
                DestroyBullet();
                //GameOver();
            }
        }
    }
}
