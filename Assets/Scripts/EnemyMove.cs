﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    public float speed = 4f;
    public float minX, maxX;

    public float startInvokeSides = 1.0f;
    public float rateInvokeSides = 1.5f;

    public float startInvokeDown = 3.0f;
    public float rateInvokeDown = 5f;

    [SerializeField]
    private bool isIntervalToMove = true;

    private bool moveToRight = true;
    private bool moveToLeft = false;

    void Start() {
        if (isIntervalToMove)
            InvokeRepeating("MoveSides", startInvokeSides, rateInvokeSides);

        InvokeRepeating("MoveDown", startInvokeDown, rateInvokeDown);
    }


    void Update()
    {
        if(!isIntervalToMove)
            MoveSides();
    }

    void MoveSides()
    {
        Vector3 temp = transform.position;

        if (moveToRight)
        {
            if (temp.x < maxX)
            {
                temp.x += speed * Time.deltaTime;
            }
            else if (temp.x >= maxX)
            {
                moveToRight = false;
                moveToLeft = true;
            }
        }


        if (moveToLeft)
        {
            if (temp.x > minX)
            {
                temp.x -= speed * Time.deltaTime;
            }
            else if (temp.x <= minX)
            {
                moveToLeft = false;
                moveToRight = true;
            }
        }


        transform.position = temp;
    }

    void MoveDown(){

        Vector3 temp = transform.position;
        temp.y -= speed * Time.deltaTime * 2;
        transform.position = temp;
    }
}
